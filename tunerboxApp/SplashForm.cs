﻿using AutoUpdaterDotNET;
using System;
using System.Drawing;
using System.Net;
using System.Windows.Forms;
using tunerboxApp.util;
using Timer = System.Windows.Forms.Timer;


namespace tunerboxApp
{
    public partial class SplashForm : Form
    {
        Reg userReg;
        Credential crd;
        Timer timer1;
        public SplashForm()
        {
            try
            {
                InitializeComponent();
                int usbstat = 0;
                USBDeviceInfo getUsb;
                getUsb = new USBDeviceInfo();
                label1.Text = "Version : " + Application.ProductVersion;
            }
            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (Globals.CheckInternet() == false)
                {
                    timer1.Stop();
                    while (Globals.CheckInternet() == false)
                    {
                        if (MessageBox.Show("Unable connect to server. Check Internet or Contact Service.", "Confirm", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                        {

                        }
                        else
                        {
                            this.Close();
                            break;
                        }
                    }
                    timer1.Start();
                }
               

                    int usbstat =Globals.VerifyUsbDvice();
                    if(usbstat==0)
                    {
                        timer1.Stop();
                        while (Globals.VerifyUsbDvice() == 0)
                        {
                            if (System.Windows.Forms.MessageBox.Show(Globals.DeviceMessage, "Confirm", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                            {
                                
                            }
                            else
                            {

                                this.Close();
                                break;
                            }

                        }
                        timer1.Start();              
    
                     }

            }

            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }
        }



        private void SplashForm_Shown(object sender, EventArgs e)
        {
            try
            {
                if (util.Globals.CheckInternet() == false)
                {
                    while (util.Globals.CheckInternet() == false)
                    {
                        if (MessageBox.Show("Unable connect to server. Check Internet or Contact Service.", "Confirm", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                        {


                        }
                        else
                        {

                            Application.Exit();

                        }
                    }
                }
                else
                {
                    crd = new Credential();
                    userReg = crd.GetUserNewMachineID();
                    Verify_device();
                }
            }
            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }
        }
        private void Verify_device()
        {

            int usbstat = 0;

            try
            {
                if (userReg == null || String.IsNullOrEmpty(userReg.email))
                {

                    if (MessageBox.Show("Do you want to Register this device ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Registration rg = new Registration();
                        rg.Show();
                        this.Hide();
                    }
                    else
                    {
                        Application.Exit();

                    }

                }
                else if (userReg.active == 0)
                {

                    DialogResult dialog = MessageBox.Show("User is not Active. Contact Support", "Error", MessageBoxButtons.OK);
                    if (dialog == DialogResult.OK)
                    {
                        Application.Exit();
                    }
                }
                else
                {
                    usbstat = userReg.GetUsbStatus();


                    //for testing;
                   // usbstat = 1;

                    if (usbstat > 0)
                    {
                        if (Globals.usbErrCnt > 10)
                        {
                            MessageBox.Show("USB device detection failed too many times !!! Contact Service", "Error");
                            Application.Exit();
                        }

                        //if (usbstat >= 0)
                        if (usbstat > 0)
                        {
                            String msg = "";
                            int isOk = 1;
                            if (userReg.pcm_Device != Globals.pcmDevice)
                            {
                                msg = "Dongle USB Device Not Found. Please insert correct USB device !";
                                label2.ForeColor = Color.Red;
                                label2.Text = "Dongle USB Device Not Found";
                                isOk = 0;

                            }
                            else
                            {
                                label2.ForeColor = Color.Green;
                                label2.Text = "Dongle USB Device Found";
                            }

                            if (userReg.scan_maticDevice != Globals.scanmaticDevice)
                            {
                                msg = "PCM tuner Device not found. Please insert correct USB device ! ";
                                label3.ForeColor = Color.Red;
                                label3.Text = "PCM tuner Device Not Found";
                                isOk = 0;
                            }
                            else
                            {
                                label2.ForeColor = Color.Green;
                                label3.Text = "PCM tuner USB Device Found";
                               
                            }

                            //for testing;
                           // isOk = 1;

                            if (isOk == 0)
                            {
                                if (MessageBox.Show(msg, "Confirm", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                                {
                                    Verify_device();

                                }
                                else
                                {
                                    // LockWorkstation.LockWorkStation();
                                    Application.Exit();
                                }
                            }
                            else
                            {

                                label2.ForeColor = Color.Green;
                                label3.ForeColor = Color.Green;

                                label2.Text = "Dongle USB Device Found";
                                label3.Text = "PCMtuner USB Device Found";


                                Credential cl = new Credential();
                                userReg.app_ver = Application.ProductVersion;
                                Globals.currentUser = userReg;
                                Globals.user_name = userReg.uname;


                                MainForm hm = new MainForm();
                                hm.Show();
                                this.Hide();

/*                                timer1 = new Timer();
                                timer1.Tick += new EventHandler(timer1_Tick);
                                timer1.Interval = 3000; // in miliseconds
                                timer1.Start();*/
                            }
                        }
                    }
                    else
                    {

                        Globals.usbErrCnt++;
                        if (MessageBox.Show("PCM tuner and Dongle USB Device not found. Please insert correct USB device !", "Confirm", MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                        {

                            label2.ForeColor = Color.Red;
                            label3.ForeColor = Color.Red;

                            label2.Text = "Dongle USB Device Not Found";
                            label3.Text = "PCM tuner Device Not Found";
                            Verify_device();

                        }
                        else
                        {
                            // LockWorkstation.LockWorkStation();
                            Application.Exit();
                        }


                    }
                }
            }

            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }

        }

        private void SplashForm_Load(object sender, EventArgs e)
        {
            AutoUpdater.ReportErrors = true;
            AutoUpdater.RunUpdateAsAdmin = false;
            AutoUpdater.ShowSkipButton = false;
            AutoUpdater.ShowRemindLaterButton = false;
            AutoUpdater.Start("https://www.tuner-box.com/versions/AutoUpdater.xml");
            AutoUpdater.CheckForUpdateEvent += AutoUpdaterOnCheckForUpdateEvent;


        }

        private void AutoUpdaterOnCheckForUpdateEvent(UpdateInfoEventArgs args)
        {
            if (args.Error == null)
            {
                if (args.IsUpdateAvailable)
                {
                    DialogResult dialogResult;
                    if (args.Mandatory.Value)
                    {
                        dialogResult =
                            MessageBox.Show(
                                $@"There is new version {args.CurrentVersion} available. You are using version {args.InstalledVersion}. This is required update. Press Ok to begin updating the application.", @"Update Available",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                    }
                    else
                    {
                        dialogResult =
                            MessageBox.Show(
                                $@"There is new version {args.CurrentVersion} available. You are using version {
                                        args.InstalledVersion
                                    }. Do you want to update the application now?", @"Update Available",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Information);
                    }

                    // Uncomment the following line if you want to show standard update dialog instead.
                    // AutoUpdater.ShowUpdateForm(args);

                    if (dialogResult.Equals(DialogResult.Yes) || dialogResult.Equals(DialogResult.OK))
                    {
                        try
                        {
                            if (AutoUpdater.DownloadUpdate(args))
                            {
                                Application.Exit();
                            }
                        }
                        catch (Exception exception)
                        {
                            MessageBox.Show(exception.Message, exception.GetType().ToString(), MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                 /*   MessageBox.Show(@"There is nooooo update available please try again later.", @"No update available",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);*/
                }
            }
            else
            {
                if (args.Error is WebException)
                {
                    MessageBox.Show(
                        @"There is a problem reaching update server. Please check your internet connection and try again later.",
                        @"Update Check Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(args.Error.Message,
                        args.Error.GetType().ToString(), MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }
    }
}
