﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using tunerboxApp.util;

namespace tunerboxApp
{
    public partial class Registration : Form
    {
        public Registration()
        {
            try
            {
                InitializeComponent();
                this.StartPosition = FormStartPosition.CenterScreen;
                List<string> L = new List<string>();
                L = Reg.GetAllCountrysNames();

                // MessageBox.Show(Reg.GetIP()+"MAC"+Reg.GetMacByIP(Reg.GetIP()) + "EIP" + Reg.GetExternalIp());

                //MessageBox.Show(Reg.GetIP());

                countryComboBox.DataSource = L;
                countryComboBox.SelectedIndex = 25;

                if (util.Globals.CheckInternet() == false)
                {
                    MessageBox.Show("Unable connect to server. Check Internet or Contact Service.");
                    this.Close();
                }
            }

            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }


            // label10.Text= "CPU:"+HardwareInfo.GetProcessorId()+" \n MB:"+HardwareInfo.GetMACAddress() + " \n HDD:" + HardwareInfo.GetHDDSerialNo();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {

            button2.Enabled = false;
            try
            {
                Boolean isValidInput = Validateform();
                if (isValidInput)
                {

                    Reg cl = new Reg();
                    String is_user_exists = cl.VeryifyUser(emailTextBox.Text.Trim(), emailTextBox.Text.Trim());


                    if (is_user_exists.Equals("1"))
                    {
                        MessageBox.Show("User Already Exists !!");
                    }

                    else if (is_user_exists.Equals("2"))
                    {

                        MessageBox.Show("User Email Already Exists !!");

                    }
                    else if (is_user_exists.Equals("3"))
                    {

                        MessageBox.Show("User Machine Already Exists !!");

                    }
                    else if (is_user_exists.Equals("-1"))
                    {

                        MessageBox.Show("Cannot Register !! Try Again Later");

                    }
                    else
                    {

                        String isok = "";
                        Reg regUser = new Reg();
                        regUser.fname = fnameTextBox.Text.Trim();
                        regUser.cname = cnameTextBox.Text.Trim();
                        regUser.email = emailTextBox.Text.Trim();
                        regUser.phone = phoneTextBox.Text.Trim();
                        regUser.country = countryComboBox.Text.Trim();
                        regUser.uname = emailTextBox.Text.Trim();
                        regUser.upass = DateTime.Today.ToString();
                        regUser.uip = Reg.GetIP();
                        regUser.eip = Reg.GetExternalIp().ToString();
                        regUser.umac = Reg.GetMacByIP(regUser.uip);

                        isok = regUser.RegisterUser();
                        if (isok.Equals("1"))
                        {
                            MessageBox.Show("User Successfully Created. Please Restart Application");
                        }
                        else if (isok.Equals("-1"))
                        {

                            MessageBox.Show("Registration Failed !! Try Again Later");

                        }
                        else
                        {
                            MessageBox.Show("User Cannot Created ! Contact Support");

                        }



                    }

                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Cannot Register !! Try Again Later" + Environment.NewLine + " " + exp.Message);


            }

            button2.Enabled = true;
        }


        public bool Validateform()
        {
            Boolean b = true;
            Regex mRegxExpression;

            try
            {
                if (string.IsNullOrEmpty(fnameTextBox.Text))
                {
                    fnameTextBox.Focus();
                    errorProvider1.SetError(fnameTextBox, "Please Enter Full Name");
                    b = false;
                }
                if (string.IsNullOrEmpty(fnameTextBox.Text))
                {
                    fnameTextBox.Focus();
                    errorProvider1.SetError(cnameTextBox, "Please Enter Compnay  Name");
                    b = false;
                }

                if (string.IsNullOrEmpty(emailTextBox.Text))
                {
                    fnameTextBox.Focus();
                    errorProvider1.SetError(emailTextBox, "Please Enter Email");
                    b = false;
                }

                if (emailTextBox.Text.Trim() != string.Empty)
                {
                    mRegxExpression = new Regex(@"^([a-zA-Z0-9_\-])([a-zA-Z0-9_\-\.]*)@(\[((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}|((([a-zA-Z0-9\-]+)\.)+))([a-zA-Z]{2,}|(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\])$");

                    if (!mRegxExpression.IsMatch(emailTextBox.Text.Trim()))
                    {
                        errorProvider1.SetError(emailTextBox, "Please Enter Valid Email Address");

                        emailTextBox.Focus();
                        b = false;
                    }
                }
                if (string.IsNullOrEmpty(phoneTextBox.Text))
                {
                    fnameTextBox.Focus();
                    errorProvider1.SetError(phoneTextBox, "Please Enter Phone Number");
                    b = false;
                }

                if (string.IsNullOrEmpty(countryComboBox.Text))
                {
                    fnameTextBox.Focus();
                    errorProvider1.SetError(countryComboBox, "Please Select Country");
                    b = false;
                }
            }

            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }



            return b;



        }


    }
}
