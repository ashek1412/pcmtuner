﻿using Microsoft.Web.WebView2.Core;
using System;
using System.Windows.Forms;
using tunerboxApp.util;

namespace tunerboxApp
{
    public partial class InstructionForm : Form
    {
        public String url { get; set; }
        public CoreWebView2Deferral Deferral { get; set; }
        public CoreWebView2NewWindowRequestedEventArgs EventArgs { get; set; }

        public InstructionForm()
        {
            try
            {

                InitializeComponent();
                IninwebView22();
            }
            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }
        }

        private void webView22_CoreWebView2InitializationCompleted(object sender, CoreWebView2InitializationCompletedEventArgs e)
        {
            try
            {
                if (e.IsSuccess)
                {
                    if (Deferral != null)
                    {
                        EventArgs.NewWindow = webView22.CoreWebView2;

                        Deferral.Complete();

                        Deferral = null;
                        EventArgs = null;
                    }
                }
                webView22.CoreWebView2.Settings.IsStatusBarEnabled = false;
                webView22.CoreWebView2.Settings.IsZoomControlEnabled = false;
            }

            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }
        }

        async void IninwebView22()
        {
            try
            {
                await webView22.EnsureCoreWebView2Async(null);
                webView22.CoreWebView2.Navigate(this.url);
            }
            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }
        }


    }
}
