﻿using System;
using System.Threading;
using System.Windows.Forms;
using tunerboxApp.util;

namespace tunerboxApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static Mutex mutex = new Mutex(false, "483dfd27 - 2aca - 4ac4 - 8bed - e6cbf3980e62");
        static Form SplashScreen;
        [STAThread]
        static void Main()
        {
            try
            {
                if (mutex.WaitOne(TimeSpan.Zero, true))
                {
                    
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

                    SplashScreen = new SplashForm();
                    Application.Run(SplashScreen);
                }
                else
                {
                    MessageBox.Show("only one instance at a time");
                }
            }
            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);

            }

        }

    }
}
