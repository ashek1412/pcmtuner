﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using tunerboxApp.util;

namespace tunerboxApp
{
    public partial class PCMflashForm : Form
    {
        Process proc = null;
        /// <summary>
		/// Track if the application has been created
		/// </summary>
		bool created = false;
        bool isFirst = false;
        IntPtr appWin;

        public PCMflashForm()
        {
            InitializeComponent();
        }

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWnChild, IntPtr hWndNewParent);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        
        [DllImport("user32.dll", EntryPoint = "GetWindowLongA", SetLastError = true)]
        private static extern long GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll", EntryPoint = "SetWindowLongA", SetLastError = true)]
        private static extern long SetWindowLong(IntPtr hwnd, int nIndex, long dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetWindowPos(IntPtr hwnd, long hWndInsertAfter, long x, long y, long cx, long cy, long wFlags);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);

        [DllImport("user32.dll", EntryPoint = "PostMessageA", SetLastError = true)]
        private static extern bool PostMessage(IntPtr hwnd, uint Msg, long wParam, long lParam);

        private const int SWP_NOOWNERZORDER = 0x200;
        private const int SWP_NOREDRAW = 0x8;
        private const int SWP_NOZORDER = 0x4;
        private const int SWP_SHOWWINDOW = 0x0040;
        private const int WS_EX_MDICHILD = 0x40;
        private const int SWP_FRAMECHANGED = 0x20;
        private const int SWP_NOACTIVATE = 0x10;
        private const int SWP_ASYNCWINDOWPOS = 0x4000;
        private const int SWP_NOMOVE = 0x2;
        private const int SWP_NOSIZE = 0x1;
        private const int GWL_STYLE = (-16);
        private const int WS_VISIBLE = 0x10000000;
        private const int WM_CLOSE = 0x10;
        private const int WS_CHILD = 0x40000000;

        private void PCMflashForm_Load(object sender, EventArgs e)
        {
            // If control needs to be initialized/created

            Process[] pname = Process.GetProcessesByName("PCMflash");
            if (pname.Length == 0)            
            {
                // Mark that control is created
                created = true;
                try
                {
                    string pcmflashPath = Globals.checkInstalled("PCMflash") + "PCMflash.exe";
                    proc = Process.Start(pcmflashPath);
                    proc.WaitForInputIdle();
                    appWin = proc.MainWindowHandle;



                    while (appWin == IntPtr.Zero)
                    {

                        Thread.Sleep(300);
                        proc.Refresh();
                    }

                    SetParent(appWin, this.Handle);

                    // Remove border and whatnot

                   


                    // Move the window to overlay it on this window
                    MoveWindow(appWin, 0, -10, this.Width, this.Height, true);
                    isFirst = true;
                    Globals.isInsPage = 1;

                }
                catch (Exception ext)
                {
                    MessageBox.Show(ext.Message);
                }
            }
            else
            {

               
            }



        }


        protected override void OnHandleDestroyed(EventArgs e)
        {
            try
            {
                // Stop the application
                if (appWin != IntPtr.Zero)
                {
                    // Post a colse message
                    //PostMessage(proc.MainWindowHandle, WM_CLOSE, 0, 0);
                    proc.Kill();

                    // Delay for it to get the message
                    Thread.Sleep(1000);

                    // Clear internal handle
                    appWin = IntPtr.Zero;
                }
            }
             
            catch (Exception ex2 )
            {


            }


         }

        private void PCMflashForm_VisibleChanged(object sender, EventArgs e)
        {
           
        }

        private void PCMflashForm_Deactivate(object sender, EventArgs e)
        {
            try
            {
                // Stop the application
                if (appWin != IntPtr.Zero)
                {
                    // Post a colse message
                    //PostMessage(proc.MainWindowHandle, WM_CLOSE, 0, 0);
                    proc.Kill();

                    // Delay for it to get the message
                    Thread.Sleep(1000);

                    // Clear internal handle
                    appWin = IntPtr.Zero;
                }
            }
            catch (Exception ex1)
            {


            }

        }
    }
}
