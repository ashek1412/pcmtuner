﻿using Microsoft.Web.WebView2.Core;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using tunerboxApp.util;

namespace tunerboxApp
{
    public partial class MainForm : Form
    {
        private String mid = null;
        private String murl = null;

        public MainForm()
        {
            try
            {


                InitializeComponent();
                mid = Globals.currentUser.machine_id;
                Globals.isInsPage = 0;

                label1.Text = "Serial number : " + util.Globals.currentUser.sl_num;
                label2.Text = "Version : " + Application.ProductVersion;

                
            }
            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }

            //IninwebView22();


        }

        private void webView21_CoreWebView2InitializationCompleted(object sender, CoreWebView2InitializationCompletedEventArgs e)
        {
            try
            {
                Debug.WriteLine("WebView_CoreWebView2InitializationCompleted");
                webView21.CoreWebView2.Settings.IsStatusBarEnabled = false;
                webView21.CoreWebView2.Settings.IsZoomControlEnabled = false;
                webView21.CoreWebView2.NewWindowRequested -= CoreWebView2_NewWindowRequested;
                webView21.CoreWebView2.NewWindowRequested += CoreWebView2_NewWindowRequested;
            }

            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }

        }

        private void CoreWebView2_NewWindowRequested(object sender, CoreWebView2NewWindowRequestedEventArgs e)
        {
            //e.NewWindow = webView21.CoreWebView2;
            //e.Handled = true;
          
            try
            {
                string curi = e.Uri;
                if (curi.Contains("pcmflash"))
                {

                        e.Handled = true;
                        panel2.Visible = Visible;

                    if (Application.OpenForms.OfType<PCMflashForm>().Count() == 1)
                        Application.OpenForms.OfType<PCMflashForm>().First().Close();


                        PCMflashForm pcm = new PCMflashForm() { Dock = DockStyle.Fill, TopLevel = false, TopMost = true };
                        this.panel2.Controls.Add(pcm);
                        //PCMflashForm pcm = new PCMflashForm();
                        pcm.Show();


                }
                else
                {



                    InstructionForm F = new InstructionForm();
                    F.url = e.Uri;
                    e.Handled = true;
                    F.Show();
                }
            }
            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                webView21.CoreWebView2InitializationCompleted += webView21_CoreWebView2InitializationCompleted;
                Debug.WriteLine("before InitializeAsync");
                await InitializeAsync();
                Debug.WriteLine("after InitializeAsync");
                if ((webView21 == null) || (webView21.CoreWebView2 == null))
                {
                    Debug.WriteLine("not ready");
                }

                murl = "https://www.tuner-box.com/tunerapp/initiate.php?mid=" + mid;

                webView21.CoreWebView2.Navigate(murl);

                //Uri url1 = new Uri(murl);
                //webView21.Source = url1;

                Debug.WriteLine("after NavigateToString");
            }

            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }
        }

        private async Task InitializeAsync()
        {
            try
            {
                Debug.WriteLine("InitializeAsync");
                await webView21.EnsureCoreWebView2Async(null);
                Debug.WriteLine("WebView2 Runtime version: " + webView21.CoreWebView2.Environment.BrowserVersionString);
            }
            catch (Exception ex1)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                MessageBox.Show(ex1.Message);
            }
        }

        private void webView21_SourceChanged(object sender, CoreWebView2SourceChangedEventArgs e)
        {
            panel2.Visible = false;
        }
    }
}
