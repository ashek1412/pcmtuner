﻿using DeviceId;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace tunerboxApp.util
{
    public class User
    {
        public string username { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }

    public class Credstat
    {
        public string stat { get; set; }
        public string user_sl { get; set; }
        public string exp_date { get; set; }
        public string lastServiceDate { get; set; }
    }

    public class UserService
    {
        public string fid { get; set; }
        public string fname { get; set; }
    }


    public class Credential
    {

        public List<User> userlist;
        public List<UserService> usrv;
        public Reg ureg;
        public string uname { get; set; }
        public string upass { get; set; }




        /*public string VeryfyCred(string uname, string upass)
        {
            // Get Token
            //var client = new RestClient("http://localhost:86/api/verify_credentials");
            try
            {
                string machine_id = GetGetMachineID();
               
               // var client = new RestClient("https://support.vz-performance.com/magic/api/verify_credentials");
                var client = new RestClient("https://www.ecu-manager.com/magic/api/verify_credentials");

                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                request.AddParameter("user", uname);
                request.AddParameter("pass", upass);
                request.AddParameter("machine_id", machine_id);


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                Credstat dynObj = serializer.Deserialize<Credstat>(response.Content);
                //var token = d.token;
                String t = dynObj.stat.ToString();
                if(t=="1")
                {
                    Globals.exp_date = Convert.ToDateTime(dynObj.exp_date);
                    Globals.user_sl = dynObj.user_sl;
                }
                return t;
            }
            catch (Exception)
            {
                return "-1";
            }

        }*/



        public List<UserService> GetUserServices(string uname)
        {

            try
            {
                // var client = new RestClient("https://support.vz-performance.com/magic/api/get_user_ecu_service");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var client = new RestClient("https://www.ecu-manager.com/magic/api/get_user_ecu_service");

                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                request.AddParameter("user", uname);
                request.AddParameter("app_ver", Application.ProductVersion);



                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                usrv = serializer.Deserialize<List<UserService>>(response.Content);

                //var token = d.token;
                //String t = dynObj.stat.ToString();
                //return usrv.Select(s => s.fid).ToList(); 
                return usrv;
            }
            catch (Exception)
            {
                return null;
            }

        }


        public string GetGetMachineID()
        {
            // grab all online interfaces
            try
            {
                string deviceId = new DeviceIdBuilder()
                    .AddOsVersion()
                    .OnWindows(windows => windows
                        .AddProcessorId()
                        .AddMotherboardSerialNumber()
                        .AddSystemDriveSerialNumber())
                    .ToString();

                return deviceId;
            }
            catch (Exception)
            {

                return null;

            }
        }


        public string GetNewDeviceID()
        {
            // grab all online interfaces
            try
            {


                string deviceid = new DeviceIdBuilder()
                    .OnWindows(windows => windows
                        .AddProcessorId()
                        .AddMotherboardSerialNumber()
                        .AddSystemDriveSerialNumber())
                        .ToString();

                return deviceid;
            }
            catch (Exception ext)
            {
                MessageBox.Show(ext.Message);

                return null;

            }
        }


        /*        public Reg GetUserMachineID()
                {
                    // grab all online interfaces
                    try
                    {

                        ureg = new Reg();
                        string did = GetGetMachineID();
                       // WriteToFile("old deviceid " + did);


                        var client = new RestClient("https://www.ecu-manager.com/magic/api/verify_desktop_user_by_mac");
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("cache-control", "no-cache");
                        request.AddHeader("content-type", "application/x-www-form-urlencoded");

                        request.AddParameter("mac_id", did);
                        request.AddParameter("local_ip", Reg.GetIP());
                        request.AddParameter("public_ip",Reg.GetExternalIp());
                        request.AddParameter("new_devid", GetNewDeviceID());




                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        IRestResponse response = client.Execute(request);

                        JavaScriptSerializer serializer = new JavaScriptSerializer();

                        ureg = new Reg();

                        ureg = serializer.Deserialize<Reg>(response.Content);


                        return ureg;
                    }
                    catch (Exception )
                    {

                        return null;

                    }
                }*/

        public Reg GetUserNewMachineID()
        {
            // grab all online interfaces
            try
            {

                ureg = new Reg();



                string did = GetNewDeviceID();

                //WriteToFile("new deviceid " + did);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var client = new RestClient("https://www.tuner-box.com/tunerapp/api/fetch_user_data.php");
                //var client = new RestClient("http://localhost/tunerapp/api/fetch_user_data.php");

                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                request.AddParameter("mac_id", did);
                request.AddParameter("local_ip", Reg.GetIP());
                request.AddParameter("public_ip", Reg.GetExternalIp());
                request.AddParameter("app_ver", Application.ProductVersion);






                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();

                ureg = new Reg();

                ureg = serializer.Deserialize<Reg>(response.Content);


                return ureg;
            }
            catch (Exception exr)
            {

                return null;

            }
        }

        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }



}
