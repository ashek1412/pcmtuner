﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace tunerboxApp.util
{
    public class Reg
    {

        public string fname { get; set; }
        public string cname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string country { get; set; }
        public string uname { get; set; }
        public string upass { get; set; }
        public string umac { get; set; }
        public string uip { get; set; }
        public string eip { get; set; }
        public string machine_id { get; set; }
        public string device_id { get; set; }
        public string device_pkey { get; set; }
        public DateTime exp_date { get; set; }
        public string sl_num { get; set; }
        public int active { get; set; }
        public string service_stime { get; set; }
        public string service_etime { get; set; }
        public string server_time { get; set; }
        public int service_count { get; set; }
        public string ecufile_limit { get; set; }
        public string app_ver { get; set; }
        public int ecu_delay_time { get; set; }
        public string pcm_Device { get; set; }
        public string scan_maticDevice { get; set; }




        public Credential usercred;


        public static List<string> GetAllCountrysNames()
        {
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            var rez = cultures.Select(cult => (new RegionInfo(cult.LCID)).EnglishName).Distinct().OrderBy(q => q).ToList();

            return rez;
        }


        public string VeryifyUser(string uname, string email)
        {

            try
            {
                usercred = new Credential();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var client = new RestClient("https://www.tuner-box.com/tunerapp/api/fetch_user_exists.php");
                //var client = new RestClient("http://localhost/tunerapp/api/fetch_user_exists.php");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                request.AddParameter("user", uname);
                request.AddParameter("email", email);
                request.AddParameter("machine_id", usercred.GetNewDeviceID());

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                Credstat dynObj = serializer.Deserialize<Credstat>(response.Content);
                //var token = d.token;
                String t = dynObj.stat.ToString();
                return t;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);

                return "-1";
            }

        }



        /*
                // Get number of new news for user
                public int GetUserNewsCount()
                {
                    try
                    {
                        var client = new RestClient("https://www.ecu-manager.com/magic/api/get_user_pending_news_count");
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("cache-control", "no-cache");
                        request.AddHeader("content-type", "application/x-www-form-urlencoded");
                        request.AddParameter("user", util.Globals.user_name);

                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        IRestResponse response = client.Execute(request);

                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        Credstat dynObj = serializer.Deserialize<Credstat>(response.Content);
                        int tc = int.Parse(dynObj.stat);

                        return tc;
                    }
                    catch (Exception xv)
                    {
                        return 0;
                    }

                }*/
        /*
                // Get number of new tickets for user
                public int GetUserTicketsCount()
                {
                    try
                    {
                        var client = new RestClient("https://www.ecu-manager.com/magic/api/get_user_pending_tickets_count");
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("cache-control", "no-cache");
                        request.AddHeader("content-type", "application/x-www-form-urlencoded");
                        request.AddParameter("user", util.Globals.user_name);

                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        IRestResponse response = client.Execute(request);

                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        Credstat dynObj = serializer.Deserialize<Credstat>(response.Content);
                        int tc = int.Parse(dynObj.stat);

                        return tc;
                    }
                    catch (Exception xv)
                    {
                        return 0;
                    }

                }
        */



        public string RegisterUser()
        {

            try
            {

                usercred = new Credential();
                string deviceID = usercred.GetNewDeviceID();

                if (deviceID.Length == 0)
                {
                    MessageBox.Show("unable to get Device ID");
                    return null;
                }
                else
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var client = new RestClient("https://www.tuner-box.com/tunerapp/api/register_user_data.php");
                    //var client = new RestClient("http://localhost/tunerapp/api/register_user_data.php");
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("cache-control", "no-cache");
                    request.AddHeader("content-type", "application/x-www-form-urlencoded");

                    request.AddParameter("user", this.uname);
                    request.AddParameter("email", this.email);
                    request.AddParameter("fname", this.fname);
                    request.AddParameter("cname", this.cname);
                    request.AddParameter("phone", this.phone);
                    request.AddParameter("country", this.country);
                    request.AddParameter("pass", this.upass);
                    request.AddParameter("mac", this.umac);
                    request.AddParameter("local_ip", this.uip);
                    request.AddParameter("public_ip", this.eip);
                    request.AddParameter("machine_id", deviceID);




                    //Globals.WriteToFile(deviceID);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    IRestResponse response = client.Execute(request);

                    Globals.WriteToFile(response.Content);

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    Credstat dynObj = serializer.Deserialize<Credstat>(response.Content);
                    //var token = d.token;

                    String t = dynObj.stat.ToString();
                    return t;
                }
            }
            catch (Exception ecp)
            {
                Globals.WriteToFile(Extensions.ExceptionInfo(ecp));

                MessageBox.Show("Cannot Register !! Try Again Later" + Environment.NewLine + " " + ecp.Message);
                return "-1";
            }

        }


        public static string GetIP()
        {
            string strHostName = "";
            strHostName = System.Net.Dns.GetHostName();

            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

            string ipaddress = System.Convert.ToString(ipEntry.AddressList[ipEntry.AddressList.Length - 1]);

            return ipaddress.ToString();

        }


        public static string GetMacByIP(string ipAddress)
        {
            // grab all online interfaces
            var query = NetworkInterface.GetAllNetworkInterfaces()
                .Where(n =>
                    n.OperationalStatus == OperationalStatus.Up && // only grabbing what's online
                    n.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                .Select(_ => new
                {
                    PhysicalAddress = _.GetPhysicalAddress(),
                    IPProperties = _.GetIPProperties(),
                });

            // grab the first interface that has a unicast address that matches your search string
            var mac = query
                .Where(q => q.IPProperties.UnicastAddresses
                    .Any(ua => ua.Address.ToString() == ipAddress))
                .FirstOrDefault()
                .PhysicalAddress;

            // return the mac address with formatting (eg "00-00-00-00-00-00")
            return String.Join("-", mac.GetAddressBytes().Select(b => b.ToString("X2")));
        }


        public static IPAddress GetExternalIp()
        {
            using (WebClient client = new WebClient())
            {
                List<String> hosts = new List<String>();
                hosts.Add("https://icanhazip.com");
                hosts.Add("https://api.ipify.org");
                hosts.Add("https://ipinfo.io/ip");
                hosts.Add("https://wtfismyip.com/text");
                hosts.Add("https://checkip.amazonaws.com/");
                hosts.Add("https://bot.whatismyipaddress.com/");
                hosts.Add("https://ipecho.net/plain");
                foreach (String host in hosts)
                {
                    try
                    {
                        String ipAdressString = client.DownloadString(host);
                        ipAdressString = ipAdressString.Replace("\n", "");
                        return IPAddress.Parse(ipAdressString);
                    }
                    catch
                    {
                    }
                }
            }
            return null;
        }


        public int GetUsbStatus()
        {
            int usbstat = 0;
            USBDeviceInfo getUsb;
            getUsb = new USBDeviceInfo();
            usbstat = getUsb.GetUSBDevices();
            return usbstat;
        }





    }
}

