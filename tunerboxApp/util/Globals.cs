﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Windows;
using System.Windows.Forms;

namespace tunerboxApp.util
{
    public static class Globals
    {

        public static string user_name { get; set; }
        public static string user_sl { get; set; }
        public static string user_macid { get; set; }
        public static DateTime exp_date { get; set; }
        public static int usbErrCnt;
        public static int news_stat;
        public static int tickets_stat;
        public static int tickets_dl_stat;
        public static int isInsPage;
        public static string dlticket { get; set; }
        public static string usbDrive { get; set; }
        public static int user_news { get; set; }
        public static int user_tickets { get; set; }
        public static string pcmDevice { get; set; }
        public static string scanmaticDevice { get; set; }
        public static string DeviceMessage { get; set; }
       



        public static List<UserService> user_service;

        public static Reg currentUser;
      


        public static bool CheckInternet()
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                using (var client = new WebClient())
                using (var stream = client.OpenRead("https://www.tuner-box.com"))
                {
                    return true;
                }
            }
            catch (Exception exp)
            {
                // MessageBox.Show(exp.Message);
                return false;
            }
        }



        public static int VerifyUsbDvice()
        {
            int usbstat;
            USBDeviceInfo userUsb=new USBDeviceInfo();
            usbstat = userUsb.GetUSBDevices();

            //Globals.WriteToFile("VerifyUsbDvice - usbstat:" + usbstat);

            int isOk = 1;

            if (usbstat > 0)
            {
                DeviceMessage = "";
               
                if (Globals.currentUser.pcm_Device.Trim() != Globals.pcmDevice.Trim())
                {
                    DeviceMessage = "Dongle USB Device Not Found. Please insert correct USB device !";
                    isOk = 0;
                   // Globals.WriteToFile("isOk: pcmDevice" + isOk);
                }

                if (Globals.currentUser.scan_maticDevice.Trim() != Globals.scanmaticDevice.Trim())
                {
                    DeviceMessage = "PCM tuner Device not found. Please insert correct USB device ! ";

                    isOk = 0;
                   // Globals.WriteToFile("isOk : scanmaticDevice" + isOk);
                }
            }
            else
            {
                DeviceMessage = "PCM tuner and Dongle USB Device not found. Please insert correct USB device !";
                return 0;

            }

            return isOk;
        }

        public static void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }


        public static string ExceptionInfo(this Exception exception)
        {

            StackFrame stackFrame = (new StackTrace(exception, true)).GetFrame(0);
            return string.Format("At line {0} column {1} in {2}: {3} {4}{3}{5}  ",
               stackFrame.GetFileLineNumber(), stackFrame.GetFileColumnNumber(),
               stackFrame.GetMethod(), Environment.NewLine, stackFrame.GetFileName(),
               exception.Message);

        }

        public static string checkInstalled(string findByName)
        {
            string displayName;
            string InstallPath;
            string registryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";

            //64 bits computer
            RegistryKey key32 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
            RegistryKey key = key32.OpenSubKey(registryKey);

            if (key != null)
            {
                foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains(findByName))
                    {

                        InstallPath = subkey.GetValue("InstallLocation").ToString();

                        return InstallPath; //or displayName

                    }
                }
                key.Close();
            }



            return null;
        }






    }
}
