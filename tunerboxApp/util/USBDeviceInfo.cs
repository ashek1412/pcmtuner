﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;

namespace tunerboxApp.util
{
    public class USBDeviceInfo
    {
        public string Name { get; set; }
        //public string Value { get; set; }
        public string DeviceID { get; private set; }
        public string PnpDeviceID { get; private set; }
        public string Description { get; private set; }
        //USBDeviceInfo usbd;
        List<USBDeviceInfo> list;




        public String GetUSBID(String usbdrive)
        {

            try
            {

                var index = new ManagementObjectSearcher("SELECT * FROM Win32_LogicalDiskToPartition").Get().Cast<ManagementObject>();
                var disks = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive").Get().Cast<ManagementObject>();
                string driveletter = usbdrive;
                string serial = "";
                var drive = (from i in index where i["Dependent"].ToString().Contains(driveletter) select i).FirstOrDefault();
                if (drive != null)
                {
                    var key = drive["Antecedent"].ToString().Split('#')[1].Split(',')[0];

                    var disk = (from d in disks
                                where
                                    d["Name"].ToString() == "\\\\.\\PHYSICALDRIVE" + key &&
                                    d["InterfaceType"].ToString() == "USB"
                                select d).FirstOrDefault();
                    serial = disk["PNPDeviceID"].ToString().Split('\\').Last();

                    return serial;
                }
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }


        public int GetUSBDevices()
        {
           
            int isUsbexists = 0;

            try
            {
                String DeviceOne = "Guardant Code";
                String DeviceTwo = "Scanmatik SM-2";

                Globals.pcmDevice = "";
                Globals.scanmaticDevice = "";

               //String DeviceOne = "Razer Viper Mini";
                //String DeviceTwo = "ADATA USB Flash Drive USB Device";

                String DeviceID = "";
                String Name = "";


                using (var mos = new ManagementObjectSearcher(@"Select * From Win32_PnPEntity Where DeviceID Like ""USB%"""))
                {
                    using (ManagementObjectCollection collection = mos.Get())
                    {
                        foreach (var device in collection)
                        {
                          

                            DeviceID = device.GetPropertyValue("DeviceId").ToString();
                            string[] dev_id = DeviceID.Split('\\');
                            DeviceID = dev_id[1].ToString();

                            //usbdiv.PnpDeviceID = (string)device.GetPropertyValue("PNPDeviceID");
                            //usbdiv.Description = (string)device.GetPropertyValue("Description");
                            Name = (string)device.GetPropertyValue("Name");
                            if(Name.Trim()==DeviceOne)
                            {
                                Globals.pcmDevice = DeviceID;
                                isUsbexists++;
                               // Globals.WriteToFile("GetUSBDevices - Global:" + Globals.pcmDevice);
                            }
                            else if (Name.Trim() == DeviceTwo)
                            {
                                Globals.scanmaticDevice = DeviceID;
                                isUsbexists++;
                              //  Globals.WriteToFile("GetUSBDevices - Global:" + Globals.pcmDevice);
                            }

                           
                        }
                    }
                }

               


            }
            catch (Exception ex1)
            {
                //Globals.WriteToFile(Extensions.ExceptionInfo(ex1));
                return -1;

            }
            
            //Globals.WriteToFile("GetUSBDevices - Global:" + isUsbexists);

            return isUsbexists;



        }






        /*public int GetUsbDrives()
        {
            list = new List<USBDeviceInfo>();
            int isUsbexists = 0;

            try
            {
                    foreach (DriveInfo drive in DriveInfo.GetDrives())
                    {
                        String Sname = null;
                        String Svol = null;
                        String usbdiv = null;

                    if (drive.DriveType == DriveType.Removable)
                    {
                        // Console.WriteLine(string.Format("({0}) {1}", drive.Name.Replace("\\", ""), drive.VolumeLabel));

                        Sname = drive.Name.Replace("\\", "");
                        Svol = drive.VolumeLabel;

                        usbdiv = GetUSBID(Sname);

                        if (!String.IsNullOrEmpty(usbdiv))
                        {

*//*                            string folder = Sname + "\\sys";
                            Directory.CreateDirectory(folder);
                            string fileName = "\\system.xml";
                            string fullPath = folder + fileName;

                            if (File.Exists(fullPath))
                            {
                                // string serial = null;
                                if (usbId.Equals(Crypto.DecryptData(usbKey, fullPath)))
                                {
                                    Globals.usbDrive = Sname;
                                    return 1;

                                }

                            }*//*

                        }
                        else
                            return 2;
                    }
                }
                return isUsbexists;

            }
            catch (Exception)
            {

                return 2;

            }

        }*/
    }
}



